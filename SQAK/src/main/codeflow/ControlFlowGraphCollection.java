package main.codeflow;

import java.util.ArrayList;

import main.activator.Activator;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import domain.controllers.SourceGraphController;

public class ControlFlowGraphCollection {
	
	private ArrayList<ControlFlowGraph> controlFlowGraphList;
	private CompilationUnit compilationUnit;
	
	public ControlFlowGraphCollection() {
		this.controlFlowGraphList = new ArrayList<>();
	} 

	public ControlFlowGraphCollection(ArrayList<ControlFlowGraph> controlFlowGraphList,
			CompilationUnit astParsingUnit) {
		super();
		this.controlFlowGraphList = controlFlowGraphList;
		this.compilationUnit = astParsingUnit;
	}

	public ArrayList<ControlFlowGraph> getControlFlowGraphList() {
		return controlFlowGraphList;
	}

	public void setControlFlowGraphList(
			ArrayList<ControlFlowGraph> controlFlowGraphList) {
		this.controlFlowGraphList = controlFlowGraphList;
	}


	public CompilationUnit getAstParsingUnit() {
		return compilationUnit;
	}

	public void setAstParsingUnit(CompilationUnit astParsingUnit) {
		this.compilationUnit = astParsingUnit;
	}
 
	public void generateAllControFlowGraphs() {

		ControlFlowGraph tempControlFlowGraph = new ControlFlowGraph();
		
		for (MethodDeclaration singleMethodDeclaration : ((TypeDeclaration) this.compilationUnit.types().get(0)).getMethods()) {
			System.out.println("Method Name: " + singleMethodDeclaration.getName().toString());
			
			tempControlFlowGraph.setMethodDeclaration(singleMethodDeclaration);
			tempControlFlowGraph.generateControlFlowGraph(this.compilationUnit);
			
			SourceGraphController sgc = Activator.getDefaultSQAKit().getSourceGraphController();
			this.controlFlowGraphList.add(new ControlFlowGraph(sgc.getSourceGraph(),singleMethodDeclaration));
		}
	}}

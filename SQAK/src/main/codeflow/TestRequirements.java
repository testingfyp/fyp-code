package main.codeflow;

import java.util.List;

import domain.constants.GraphCoverageCriteriaId;
import adt.graph.AbstractPath;
import adt.graph.Graph;

public class TestRequirements {
	private Graph<Integer> cfg;
	private GraphCoverageCriteriaId coverageCriteriaId;
	private List<AbstractPath<Integer>> testRequirements;
	
	public TestRequirements() {
		super();
	}
	public TestRequirements(Graph<Integer> cfg,
			GraphCoverageCriteriaId coverageCriteriaId,
			List<AbstractPath<Integer>> testRequirements) {
		super();
		this.cfg = cfg;
		this.coverageCriteriaId = coverageCriteriaId;
		this.testRequirements = testRequirements;
	}
	public Graph<Integer> getCfg() {
		return cfg;
	}
	public void setCfg(Graph<Integer> cfg) {
		this.cfg = cfg;
	}
	public GraphCoverageCriteriaId getCoverageCriteriaId() {
		return coverageCriteriaId;
	}
	public void setCoverageCriteriaId(GraphCoverageCriteriaId coverageCriteriaId) {
		this.coverageCriteriaId = coverageCriteriaId;
	}
	public List<AbstractPath<Integer>> getTestRequirements() {
		return testRequirements;
	} 
	public void setTestRequirements(List<AbstractPath<Integer>> testRequirements) {
		this.testRequirements = testRequirements;
	}
	
	public void addTestRequirement(AbstractPath<Integer> requirement){
		this.testRequirements.add(requirement);
	}

}

package main.codeflow;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.core.runtime.*;

public class ASTParsing {
	
	private String filePath;
	private String classCode;
	private ASTParser astParser;
	
	public ASTParsing() {
	}

	public ASTParsing(String filePath, String classCode, ASTParser astParser) {
		super();
		this.filePath = filePath;
		this.classCode = classCode;
		this.astParser = astParser;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public ASTParser getAstParser() {
		return astParser;
	}

	public void setAstParser(ASTParser astParser) {
		this.astParser = astParser;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public CompilationUnit createAST() throws IOException{
		parseFileToStringCode();
		
		ASTParser parser = ASTParser.newParser(AST.JLS4);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setSource(this.classCode.toCharArray());
		parser.setResolveBindings(true);
		CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		return cu;
	}
	private String parseFileToStringCode() throws IOException {
		this.classCode = readFileToString(this.filePath);
		return this.classCode;
	}
	
	//****************************************************************
	//read file content into a string
	public String readFileToString(String filePath) throws IOException {
		StringBuilder fileData = new StringBuilder();//?1000
		BufferedReader reader = new BufferedReader(new FileReader(filePath));

		char[] buf = new char[10];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}
		reader.close();
		
		return  fileData.toString();	
	}

	//****************************************************************
}

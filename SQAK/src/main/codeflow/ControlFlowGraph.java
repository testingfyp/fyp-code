package main.codeflow;

import main.activator.Activator;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;

import domain.controllers.SourceGraphController;
import adt.graph.Graph;
import adt.graph.Node;

public class ControlFlowGraph {

	private MethodDeclaration methodDeclaration;
	private Graph<Integer> controlFlowGraph;

	public ControlFlowGraph() {
		super();
		this.controlFlowGraph =new Graph<>();
	} 
	public ControlFlowGraph(Graph<Integer> cfg, MethodDeclaration methodDeclaration) {
		super();
		this.controlFlowGraph = cfg;
		this.methodDeclaration = methodDeclaration;
	} 
	public void generateControlFlowGraph(CompilationUnit cu) {

		SourceGraphController sgc = Activator.getDefaultSQAKit().getSourceGraphController();
		sgc.create(cu, this.methodDeclaration.getName().toString());
		this.controlFlowGraph = sgc.getSourceGraph();
	}

	public Graph<Integer> getControlFlowGraph() {
		return controlFlowGraph;
	}

	public void setControlFlowGraph(Graph<Integer> controlFlowGraph) {
		this.controlFlowGraph = controlFlowGraph;
	}

	
	public MethodDeclaration getMethodDeclaration() {
		return methodDeclaration;
	}
	

	public void setMethodDeclaration(MethodDeclaration methodDeclaration) {
		this.methodDeclaration = methodDeclaration;
	}
}

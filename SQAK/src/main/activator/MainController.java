package main.activator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import main.codeflow.ASTParsing;
import main.codeflow.ControlFlowGraph;
import main.codeflow.ControlFlowGraphCollection;

import org.eclipse.jdt.core.dom.CompilationUnit;

import adt.graph.AbstractPath;
import adt.graph.Node;
import testSuit.Variable;
import testsuitgeneration.MethodTestSuit;
import testsuitgeneration.TestCase;
import domain.constants.GraphCoverageCriteriaId;
import domain.controllers.TestRequirementController;

public class MainController {

	public MainController(ArrayList<String> paths,
			GraphCoverageCriteriaId coverageCriteriaId) throws IOException {

		for (String filePath : paths) {
			System.out.println("\nClass Name : "
					+ filePath.substring(filePath.lastIndexOf("\\") + 1,
							filePath.length()) + "\n");
			ASTParsing astParsing = new ASTParsing();
			astParsing.setFilePath(filePath);
			CompilationUnit cu = astParsing.createAST();

			ControlFlowGraphCollection controlFlowGraphCollection = new ControlFlowGraphCollection();
			controlFlowGraphCollection.setAstParsingUnit(cu);
			controlFlowGraphCollection.generateAllControFlowGraphs();

			ArrayList<ControlFlowGraph> cfgList = controlFlowGraphCollection
					.getControlFlowGraphList();
			for (ControlFlowGraph path : cfgList) {
				// System.out.println(path.getMethodDeclaration().getName().toString()+
				// "num of nodes :"+path.getControlFlowGraph().size());
			}

			TestRequirementController trc = Activator.getDefaultSQAKit()
					.getTestRequirementController(); // gets cfg

			for (ControlFlowGraph cfg : cfgList) {
				MethodTestSuit methodTestSuit = new MethodTestSuit();

				trc.setSourceGraph(cfg.getControlFlowGraph());
				trc.selectCoverageCriteria(coverageCriteriaId); // select
																// coverage
																// criteria
				trc.generateTestRequirement(); // generate standard requirement
												// set
				Iterable<AbstractPath<Integer>> testReqSet = trc
						.getTestRequirements();

				methodTestSuit.setCoverageCriteriaId(coverageCriteriaId);
				methodTestSuit.setTestReqiurementsList(testReqSet);
				methodTestSuit.setMethodDeclaration(cfg.getMethodDeclaration());

				methodTestSuit.generateTestSuit();

				// System.out.println(methodTestSuit.printTestSuit());
			}
		}
	}

	private ArrayList<Variable> getVariablesList(String constraint) {

		ArrayList<Variable> varList= new ArrayList<Variable>();

		if(constraint.contains(">"))
			constraint = constraint.substring(0, constraint.indexOf(">")-1);
		else if(constraint.contains("<"))
			constraint = constraint.substring(0, constraint.indexOf("<")-1);
		else if(constraint.contains("="))
			constraint = constraint.substring(0, constraint.indexOf("=")-1);
		else if(constraint.contains("!"))
			constraint = constraint.substring(0, constraint.indexOf("!")-1);
		else {}
		
		String[] sa = constraint.split("\\-|\\*|\\+|\\/|\\^|\\(|\\)");
		for(String vName : sa){
			Variable v=  new Variable(vName, "int");
				if(!v.isContains(varList, v) && !vName.equals("") && !isNumeric(vName)){
				varList.add(v);
			}
		}

		for(Variable vName : varList){
			System.out.println(vName.getVariableType()+" "+ vName.getVariableName());			
		}

		return varList;
	}
	public static boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    double d = Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
	
	public static void main(String[] args) throws IOException {
/*		
		  ASTParsing astParsing = new ASTParsing(); astParsing.setFilePath(
		  "C:/Users/Asad Chachar/Desktop/SQAK backup 2/src/A.java");
		  CompilationUnit cu = astParsing.createAST();
		  
		  ControlFlowGraphCollection controlFlowGraphCollection = new
		  ControlFlowGraphCollection();
		  controlFlowGraphCollection.setAstParsingUnit(cu);
		  controlFlowGraphCollection.generateAllControFlowGraphs();
		  
		  ArrayList<ControlFlowGraph> cfgList =
		  controlFlowGraphCollection.getControlFlowGraphList();
		  for(ControlFlowGraph path : cfgList){
		  System.out.println("num of nodes :"
		  +path.getControlFlowGraph().size()); }
		  
		  TestRequirementController trc =
		  Activator.getDefaultSQAKit().getTestRequirementController(); // gets cfg
		  
		  for(ControlFlowGraph cfg : cfgList){ MethodTestSuit methodTestSuit =
		  new MethodTestSuit();
		 
		  trc.setSourceGraph(cfg.getControlFlowGraph());
		  trc.selectCoverageCriteria(GraphCoverageCriteriaId.PRIME_PATH);//
		  select coverage criteria trc.generateTestRequirement(); // generate
		  standard requirement set Iterable<AbstractPath<Integer>> testReqSet =
		  trc.getTestRequirements();
		  
		  methodTestSuit.setCoverageCriteriaId(GraphCoverageCriteriaId.PRIME_PATH
		  ); methodTestSuit.setTestReqiurementsList(testReqSet);
		  methodTestSuit.setMethodDeclaration(cfg.getMethodDeclaration());
		  
		  methodTestSuit.generateTestSuit();
		  
		  }
*/	 
	}
}
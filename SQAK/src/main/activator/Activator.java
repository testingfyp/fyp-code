package main.activator;

import domain.controllers.BytemanController;
import domain.controllers.CoverageDataController;
import domain.controllers.DefUsesController;
import domain.controllers.SourceGraphController;
import domain.controllers.TestPathController;
import domain.controllers.TestRequirementController;

public class Activator  {

	private static SQAKit sQAKit = new SQAKit();
	

	public Activator() {
		sQAKit = new SQAKit(); 
	}
	
	public static SQAKit getDefaultSQAKit() {
		return sQAKit; 
	}

    
    public SourceGraphController getSourceGraphController() {
		return sQAKit.getSourceGraphController();
	}
    
    public TestRequirementController getTestRequirementController() {
		return sQAKit.getTestRequirementController();
	}
    
    public TestPathController getTestPathController() {
		return sQAKit.getTestPathController();
	}
    
    public CoverageDataController getCoverageDataController() {
		return sQAKit.getCoverageDataController();
	}
    
	public DefUsesController getDefUsesController() {
		return sQAKit.getDefUsesController();
	}
	
	public BytemanController getBytemanController() {
		return sQAKit.getBytemanController();
	}

}
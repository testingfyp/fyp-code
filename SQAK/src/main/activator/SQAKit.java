package main.activator;

/*import ui.StatisticsSet;
import ui.controllers.CFGController;
import ui.controllers.EditorController;
import ui.controllers.StatisticsController;
import ui.controllers.ViewController;
*/import main.codeflow.ControlFlowGraphCollection;
import domain.CoverageDataSet;
import domain.DefUsesSet;
import domain.SourceGraph;
import domain.TestPathSet;
import domain.TestRequirementSet;
import domain.constants.Layer;
import domain.constants.TourType;
import domain.controllers.BytemanController;
import domain.controllers.CoverageDataController;
import domain.controllers.DefUsesController;
import domain.controllers.SourceGraphController;
import domain.controllers.TestPathController;
import domain.controllers.TestRequirementController;


public class SQAKit { 

	private SourceGraphController sourceGraphController;
	private TestRequirementController testRequirementController;
	private TestPathController testPathController;
	private CoverageDataController coverageDataController;
	private DefUsesController defusesController;
	private BytemanController bytemanController;
	
	private ControlFlowGraphCollection controlFlowGraphCollection;	//added
	
	public SQAKit() {
		SourceGraph sourceGraph = new SourceGraph();
		sourceGraphController = new SourceGraphController(sourceGraph);
		testRequirementController = new TestRequirementController(sourceGraph, new TestRequirementSet());
		testPathController = new TestPathController(new TestPathSet());
		testPathController.selectTourType(TourType.TOUR.toString());
		coverageDataController = new CoverageDataController(new CoverageDataSet());
		defusesController = new DefUsesController(new DefUsesSet());
		bytemanController = new BytemanController();
		
		this.controlFlowGraphCollection = new ControlFlowGraphCollection();
	}
	
	public SourceGraphController getSourceGraphController() {
		return sourceGraphController;
	}
	
	public TestRequirementController getTestRequirementController() {
		return testRequirementController;
	}
	
	public TestPathController getTestPathController() {
		return testPathController;
	}
	
	public CoverageDataController getCoverageDataController() {
		return coverageDataController;
	}
	
	
	public DefUsesController getDefUsesController() {
		return defusesController;
	}

	public BytemanController getBytemanController() {
		return bytemanController;
	}

	
	public ControlFlowGraphCollection getControlFlowGraphCollection() {
		return controlFlowGraphCollection;
	}

	public void setControlFlowGraphCollection(
			ControlFlowGraphCollection controlFlowGraphCollection) {
		this.controlFlowGraphCollection = controlFlowGraphCollection;
	}

}
public class B {
	private int currentNode;
	private int successorNode;
	private String guard;
	private boolean decision;

	public B() {
		// TODO Auto-generated constructor stub
	}

	public B(int currentNode, int successorNode, String guard,
			boolean decision) {
		setCurrentNode(currentNode);
		setSuccessorNode(successorNode);
		setDecission(decision);
		setGuard(guard);

	}

	public void setCurrentNode(int currentNode) {
		this.currentNode = currentNode;
	}

	public void setDecission(boolean decision) {
		this.decision = decision;
	}

	public void setGuard(String guard) {
		this.guard = guard;
	}

	public void setSuccessorNode(int successorNode) {
		this.successorNode = successorNode;
	}

	public boolean isDecission() {
		return decision;
	}

	public int getCurrentNode() {
		return currentNode;
	}

	public int getSuccessorNode() {
		return successorNode;
	}

	public String getGuard() {
		return guard;
	}

	public boolean getDecision() {
		return this.decision;
	}

	public void print() {
		System.out.println(this.getCurrentNode() + " "
				+ this.getSuccessorNode() + " " + this.getGuard() + " "
				+ this.getDecision());
	}
}

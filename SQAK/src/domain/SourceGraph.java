package domain;

import java.util.Iterator;
import java.util.List;
import java.util.Observable;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;


//import ui.constants.JavadocTagAnnotations;
import adt.graph.Graph;
import adt.graph.Node;
import domain.ast.visitors.GraphBuilder;
import domain.constants.Layer;
import domain.events.CFGCreateEvent;
import domain.events.CFGUpdateEvent;
import domain.graph.visitors.IGraphVisitor;

public class SourceGraph extends Observable {
	
	private Graph<Integer> ggraph;
	private byte[] hash;
	private List<SingleVariableDeclaration> params;
	private List<VariableDeclarationFragment> attributes;
	private List<EnumDeclaration> enumFields;
	
	public SourceGraph() {
		ggraph = new Graph<Integer>();
	}
	
	public void create(ICompilationUnit unit, String methodName) {
		// Now create the AST for the ICompilationUnits
		CompilationUnit compilationUnit = parse(unit);
		GraphBuilder graphBuilder = new GraphBuilder(methodName, compilationUnit);
		compilationUnit.accept(graphBuilder);
		ggraph = graphBuilder.getGraph();
		hash = graphBuilder.getMethodHash();
		params = graphBuilder.getMethodParameters();
		attributes = graphBuilder.getClassAttributes();
		enumFields = graphBuilder.getEnumClassAttributes();
		setChanged();
		notifyObservers(new CFGCreateEvent(ggraph));
		
	}
	
	public void create(CompilationUnit compilationUnit, String methodName) {
		// Now create the AST for the ICompilationUnits
	//	CompilationUnit compilationUnit = parse(unit);
		GraphBuilder graphBuilder = new GraphBuilder(methodName, compilationUnit);
		compilationUnit.accept(graphBuilder);
		ggraph = graphBuilder.getGraph();
		hash = graphBuilder.getMethodHash();
		params = graphBuilder.getMethodParameters();
		attributes = graphBuilder.getClassAttributes();
		enumFields = graphBuilder.getEnumClassAttributes();
		setChanged();
		notifyObservers(new CFGCreateEvent(ggraph));
	}
	public Graph<Integer> getSourceGraph() {
		return ggraph;
	}
	
	public int numberOfNodes() {
		return ggraph.size(); 
	}
	
	public List<SingleVariableDeclaration> getMethodParameters() {
		return params;
	}
	
	public List<VariableDeclarationFragment> getClassAttributes() {
		return attributes;
	}
	
	public List<EnumDeclaration> getEnumClassAttributes() {
		return enumFields;
	}
	
	public CompilationUnit getCompilationUnit(ICompilationUnit unit) {
		return parse(unit);
	}
	
	/*public Map<JavadocTagAnnotations, List<String>> getJavadocAnnotations() {
		return javadocAnnotations;
	}
	*/
	public byte[] getMethodHash() {
		return hash;
	}
	
	public void applyVisitor(IGraphVisitor<Integer> visitor) {
		ggraph.accept(visitor);		
	}
	
	@SuppressWarnings("deprecation")
	private static CompilationUnit parse(ICompilationUnit unit) {
		ASTParser parser = ASTParser.newParser(AST.JLS3);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setSource(unit);
		parser.setResolveBindings(true);
		return (CompilationUnit) parser.createAST(null); 
	}

	public void updateMetadataInformation(Graph<Integer> graph) {
		graph.selectMetadataLayer(Layer.INSTRUCTIONS.getLayer());
		ggraph.selectMetadataLayer(Layer.INSTRUCTIONS.getLayer());
		Iterator<Node<Integer>> sourceGraphIt = ggraph.getNodes().iterator();
		Iterator<Node<Integer>> graphIt = graph.getNodes().iterator();
		while(sourceGraphIt.hasNext() && graphIt.hasNext()) {
			Node<Integer> gNode = graphIt.next();
			Node<Integer> sgNode  = sourceGraphIt.next();
			ggraph.addMetadata(sgNode, null);
			ggraph.addMetadata(sgNode, graph.getMetadata(gNode));
		}
		setChanged();
		notifyObservers(new CFGUpdateEvent(ggraph));
	}

	public Graph<Integer> getGraph() {
		return ggraph;
	}

	public void setGraph(Graph<Integer> ggraph) {
		this.ggraph = ggraph; 
	}
	
}
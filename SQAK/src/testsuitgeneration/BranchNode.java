package testsuitgeneration;
public class BranchNode {
	private Integer currentNode;
	private Integer successorNode;
	private String guard;
	private boolean decision;

	public BranchNode() {
		// TODO Auto-generated constructor stub
	}

	public BranchNode(Integer currentNode, Integer successorNode, String guard,
			boolean decision) {
		setCurrentNode(currentNode);
		setSuccessorNode(successorNode);
		setDecission(decision);
		setGuard(guard);

	}

	public void setCurrentNode(Integer currentNode) {
		this.currentNode = currentNode;
	}

	public void setDecission(boolean decision) {
		this.decision = decision;
	}

	public void setGuard(String guard) {
		this.guard = guard;
	}

	public void setSuccessorNode(Integer successorNode) {
		this.successorNode = successorNode;
	}

	public boolean isDecission() {
		return decision;
	}

	public int getCurrentNode() {
		return currentNode;
	}

	public int getSuccessorNode() {
		return successorNode;
	}

	public String getGuard() {
		return guard;
	}

	public boolean getDecision() {
		return this.decision;
	}

	public void print() {
		System.out.println(this.getCurrentNode() + " "
				+ this.getSuccessorNode() + " " + this.getGuard() + " "
				+ this.getDecision());
	}
}

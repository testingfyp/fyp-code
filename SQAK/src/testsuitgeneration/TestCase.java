package testsuitgeneration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import main.activator.Activator;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.jdt.core.dom.WhileStatement;

import editor.Line;
import adt.graph.Node;
import testSuit.Assignment;
import testSuit.BackTracker;
import testSuit.Constraint;
import testSuit.Variable;

public class TestCase {

	private List<VariableAndValue> variableAndValueList;

	public TestCase() {
		this.variableAndValueList = new ArrayList<>();
	}

	public List<VariableAndValue> getVariableAndValueList() {
		return variableAndValueList;
	}
	public TestCase getTestCase() {
		return this;
	}

	public void setVariableAndValueList(List<VariableAndValue> variableAndValueList) {
		this.variableAndValueList = variableAndValueList;
	}

	public List<VariableAndValue> getVariableList() {
		return variableAndValueList;
	}

	public void setVariableList(List<VariableAndValue> variableList) {
		this.variableAndValueList = variableList;
	}

	public void generateTestcase(MethodDeclaration methodDeclaration, String testRequirement,ArrayList<BranchNode> branchNodeList) {

		ArrayList<Integer> nodeNumbersList = getNodeNumbersList(testRequirement);

		ArrayList<Variable> parametersList = getParametersList(methodDeclaration);
		ArrayList<Constraint> constraintsList = getConstraintsList(methodDeclaration,nodeNumbersList,branchNodeList); // set of constraints

		BackTracker backTracker = new BackTracker(constraintsList, parametersList);
		Assignment finalAssignment = backTracker.BackTrackingSearch();
		setVariableAndValueList(finalAssignment);
	}

	private ArrayList<Variable> getParametersList(MethodDeclaration methodDeclaration) {
		ArrayList<Variable> arrayList = new ArrayList<>();

		for (Object parameter : methodDeclaration.parameters()) {
			Variable var = new Variable();
			var.setVariableName(parameter.toString().substring(
					parameter.toString().lastIndexOf(" ") + 1));

			VariableDeclaration variableDeclaration = (VariableDeclaration) parameter;
			String type = variableDeclaration.getStructuralProperty(
					SingleVariableDeclaration.TYPE_PROPERTY).toString();
			/*
			 * for (int i = 0; i < variableDeclaration.getExtraDimensions();
			 * i++) { type += "[]"; }
			 */
			var.setVariableType(type);

			arrayList.add(var);
		}
		return arrayList;
	}

	private ArrayList<Constraint> getConstraintsList(
			MethodDeclaration methodDeclaration,
			ArrayList<Integer> nodeNumbersList,
			ArrayList<BranchNode> branchNodeList) 
			{
		ArrayList<Constraint> constraintList = new ArrayList<>();

		for (Integer nodeVal : nodeNumbersList) {
			Node<Integer> tempNode = Activator.getDefaultSQAKit()
					.getSourceGraphController().getSourceGraph()
					.getNode(nodeVal);
			String guard = ifNodeIsConditiongetGuard(tempNode);
			if (!guard.equals("null")) {
				for (BranchNode branchNode : branchNodeList) {
					if (branchNode.getCurrentNode() == tempNode.getValue()) {
						int ind = nodeNumbersList.indexOf(tempNode);
						if (branchNode.getSuccessorNode() == nodeNumbersList
								.get(ind + 1)) {
							String con = branchNode.getGuard();
							if (branchNode.getDecision() == false) {
								Boolean flag = false;
								Constraint c1 = getConstraint(
										methodDeclaration, guard, flag);
								constraintList.add(c1);

							} else {
								Boolean flag = true;
								Constraint c1 = getConstraint(
										methodDeclaration, guard, flag);
								constraintList.add(c1);

							}
						}
					}
				}
			}
		}
		return constraintList;
	}

	public Constraint getConstraint(MethodDeclaration methodDeclaration, String guard, Boolean signal) {
		ArrayList<Variable> variableList = getParametersList(methodDeclaration);
		Variable var1 = null;
		Variable var2 = null;

		guard.replaceAll(" ", "");

		String operator = "";
		String operand1 = "";
		String operand2 = "";

		boolean flag = false;
		for (Character ch : guard.toCharArray()) {
			if (!Character.isAlphabetic(ch) && !Character.isDigit(ch)
					&& !ch.equals('_')) {
				operator += ch.toString();
				flag = true;
				continue;
			}
			if (!flag)
				operand1 += ch;
			else
				operand2 += ch;
		}
		operand1 = operand1.replaceAll(" ", "");
		operator = operator.replaceAll(" ", "");
		operand2 = operand2.replaceAll(" ", "");
		/*
		 * System.out.println(operand1); System.out.println(operand2);
		 * System.out.println(operator);
		 */

		for (int i = 0; i < variableList.size(); i++) {
			if (variableList.get(i).getVariableName().equals(operand1)) {
				var1 = variableList.get(i);
			}
		}
		for (int i = 0; i < variableList.size(); i++) {
			if (variableList.get(i).getVariableName().equals(operand2)) {
				var2 = variableList.get(i);
			}
		}
		if(signal==false)
		{
			if(operator.contains("<"))
			{
				operator.replace("<", ">");
			}
		}

		Constraint constraint = new Constraint(var1, var2, operator);
		return constraint;
	}

	private String ifNodeIsConditiongetGuard(Node<Integer> node) {
		@SuppressWarnings("unchecked")
		HashMap<ASTNode, Line> nodeInstructions = (HashMap<ASTNode, Line>) Activator
				.getDefaultSQAKit().getSourceGraphController().getSourceGraph()
				.getMetadata(node);

		if (nodeInstructions != null) {
			List<ASTNode> astNodes = getASTNodes(nodeInstructions);

			switch (astNodes.get(0).getNodeType()) {
			case ASTNode.IF_STATEMENT:
				IfStatement ifStatement = (IfStatement) astNodes.get(0);
				return ifStatement.getExpression().toString();
			case ASTNode.DO_STATEMENT:
				DoStatement doStatement = (DoStatement) astNodes.get(0);
				return doStatement.getExpression().toString();
			case ASTNode.FOR_STATEMENT:
				ForStatement forStatement = (ForStatement) astNodes.get(0);
				return forStatement.getExpression().toString();
			case ASTNode.ENHANCED_FOR_STATEMENT:
				EnhancedForStatement enhancedForStatement = (EnhancedForStatement) astNodes
						.get(0);
				return enhancedForStatement.getExpression().toString();
			case ASTNode.SWITCH_STATEMENT:
				SwitchStatement switchStatement = (SwitchStatement) astNodes
						.get(0);
				return switchStatement.getExpression().toString();
			case ASTNode.WHILE_STATEMENT:
				WhileStatement whileStatement = (WhileStatement) astNodes
						.get(0);
				return whileStatement.getExpression().toString();
			}
		}
		return "null";
	}

	private  List<ASTNode> getASTNodes(HashMap<ASTNode, Line> map) {
		List<ASTNode> nodes = new LinkedList<ASTNode>();
		for (Entry<ASTNode, Line> entry : map.entrySet())
			nodes.add(entry.getKey());
		return nodes;
	}

	private ArrayList<Integer> getNodeNumbersList(String testRequirement) {
		ArrayList<Integer> nodeNumsList = new ArrayList<Integer>();
		for (int i = 0; i < testRequirement.length(); i++) { // [1,2,3,4]
			if (Character.isDigit(testRequirement.charAt(i)))
				nodeNumsList.add(Integer.parseInt(""
						+ testRequirement.charAt(i) + ""));
		}
		return nodeNumsList;
	}

	private void setVariableAndValueList(Assignment assignment){
		
		for(Map.Entry<Variable, Integer> entry: Assignment.getAssignmentObject().getAssignedValues().entrySet()){
			this.variableAndValueList.add(new VariableAndValue((Variable)entry.getKey(), (Integer)entry.getValue()));
		}
	}

	public String printTestCase(){
		String testCaseString = "(";
		for(VariableAndValue variableAndValue : this.variableAndValueList){
			testCaseString += variableAndValue.getVariableName() + " : "+ variableAndValue.getValue() +" ";
		}
		testCaseString+=")";
		return testCaseString;
	}
}

package testsuitgeneration;


import testSuit.Variable;

public class VariableAndValue {

	private Variable variable;
	private Object value;

	public VariableAndValue() {
		this.variable = new Variable();
		this.value = new Object();
	}

	public VariableAndValue(Variable variable, Object value) {
		super();
		this.variable = variable;
		this.value = value;
	}

	public String getVariableName() {
		return this.variable.getVariableName();
		}

	public String getVariableType() {
		return this.variable.getVariableType();
	}

	public Variable getVariableDeclaration() {
		return variable;
	}

	public void setVariable(Variable variable) {
		this.variable = variable;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}

package testsuitgeneration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import main.activator.Activator;
import main.codeflow.TestRequirements;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.WhileStatement;

import domain.constants.GraphCoverageCriteriaId;
import editor.Line;
import adt.graph.AbstractPath;
import adt.graph.Node;

public class MethodTestSuit {

	private List<String> testReqiurementsList;
	private List<TestCase> testCasesList;

	private MethodDeclaration methodDeclaration;
	private GraphCoverageCriteriaId coverageCriteriaId;
	
	

	public MethodTestSuit() {
		this.methodDeclaration = null;
		this.coverageCriteriaId = null;
		this.testReqiurementsList = new ArrayList<String>();
		this.testCasesList = new ArrayList<TestCase>();
	}

	public ArrayList<TestCase> generateTestSuit() {

//************* populate decision list **********************		
		ArrayList<BranchNode> branchNodeList = new ArrayList<>();

		for(String requirement : this.testReqiurementsList)
		 {
			 ArrayList<Node<Integer>> nodeIntegerList = getNodesList(requirement);
			 for (Node<Integer> node: nodeIntegerList) {
				 	String guard = ifNodeIsConditiongetGuard(node);
					if(!guard.equals("null"))	// when guard node occurs
						branchDecision(branchNodeList, node,nodeIntegerList.get(nodeIntegerList.indexOf(node)+1),guard);
				 }
		 }
		 for(BranchNode branchNode : branchNodeList){	//print
			 branchNode.print();
		 }
//********************************************************		
		 
		String temp=null;
		ArrayList<TestCase> testSuits = new ArrayList<TestCase>();
		System.out.println(this.methodDeclaration.getName().toString());
		 for(String requirement : this.testReqiurementsList)
		 {
			TestCase testCase = new TestCase();
			System.out.print(requirement);
			testCase.generateTestcase(methodDeclaration, requirement,branchNodeList);
			this.testCasesList.add(testCase);
		 }
		
		return testSuits;
	}

	
	private void branchDecision(ArrayList<BranchNode> branchNodeList, Node<Integer> currentNode, Node<Integer> successorNode,String guard)
	{
		if(branchNodeList.isEmpty())
			branchNodeList.add(new BranchNode(currentNode.getValue(), successorNode.getValue(), guard, true));

		boolean decision = false;
		for(BranchNode branchNode : branchNodeList){
			if(branchNode.getCurrentNode() == currentNode.getValue()){
				if(branchNode.getSuccessorNode() == successorNode.getValue())
					return ;
				else{decision = false;}
			}
			else{decision = true;}
		}
		if(decision)
			branchNodeList.add(new BranchNode(currentNode.getValue(), successorNode.getValue(), guard, true));
		else
			branchNodeList.add(new BranchNode(currentNode.getValue(), successorNode.getValue(), guard, false));
			
		return ;
	}

	public List<String> getTestReqiurementsList() {
		return testReqiurementsList;
	}

	public void setTestReqiurementsList(Iterable<AbstractPath<Integer>> testRequirements) {
		for (AbstractPath<Integer> requirement : testRequirements) {
			if (isInitialRequirement(requirement.toString()))
				this.testReqiurementsList.add(requirement.toString()); // "[0,1,3,4,5]");
		}
	}

	public List<TestCase> getTestCasesList() {
		return testCasesList;
	}

	public void setTestCasesList(List<TestCase> testCasesList) {
		this.testCasesList = testCasesList;
	}

	// ***************************************************************
	private static boolean isInitialRequirement(String string) {
		if (string.charAt(1) == '0')
			return true;
		return false;
	}
	// ******************** setter getter **********************
	public MethodDeclaration getMethodDeclaration() {
		return methodDeclaration;
	}

	public void setMethodDeclaration(MethodDeclaration methodDeclaration) {
		this.methodDeclaration = methodDeclaration;
	}

	public GraphCoverageCriteriaId getCoverageCriteriaId() {
		return coverageCriteriaId;
	}

	public void setCoverageCriteriaId(GraphCoverageCriteriaId coverageCriteriaId) {
		this.coverageCriteriaId = coverageCriteriaId;
	}
	
	public String printTestSuit(){
		String testuitString = "[\n";
		for(TestCase testCase : this.testCasesList){
			testuitString += testCase.printTestCase()+"\n";
		}
		testuitString += "]";
		return testuitString;
	}
	public ArrayList<Node<Integer>> getNodesList(String testRequirement){
		ArrayList<Node<Integer>> nodesList = new ArrayList<>();
		for (int i = 0; i < testRequirement.length(); i++) { // [1,2,3,4]
			if (Character.isDigit(testRequirement.charAt(i)))
				nodesList.add(Activator.getDefaultSQAKit().getSourceGraphController().getSourceGraph().getNode(Integer.parseInt(""+ testRequirement.charAt(i) + "")));
		}
		return nodesList;
	}
	
	private String ifNodeIsConditiongetGuard(Node<Integer> node) {
		@SuppressWarnings("unchecked")
		HashMap<ASTNode, Line> nodeInstructions = (HashMap<ASTNode, Line>) Activator
				.getDefaultSQAKit().getSourceGraphController().getSourceGraph()
				.getMetadata(node);

		if (nodeInstructions != null) {
			List<ASTNode> astNodes = getASTNodes(nodeInstructions);

			switch (astNodes.get(0).getNodeType()) {
			case ASTNode.IF_STATEMENT:
				IfStatement ifStatement = (IfStatement) astNodes.get(0);
				return ifStatement.getExpression().toString();
			case ASTNode.DO_STATEMENT:
				DoStatement doStatement = (DoStatement) astNodes.get(0);
				return doStatement.getExpression().toString();
			case ASTNode.FOR_STATEMENT:
				ForStatement forStatement = (ForStatement) astNodes.get(0);
				return forStatement.getExpression().toString();
			case ASTNode.ENHANCED_FOR_STATEMENT:
				EnhancedForStatement enhancedForStatement = (EnhancedForStatement) astNodes
						.get(0);
				return enhancedForStatement.getExpression().toString();
			case ASTNode.SWITCH_STATEMENT:
				SwitchStatement switchStatement = (SwitchStatement) astNodes
						.get(0);
				return switchStatement.getExpression().toString();
			case ASTNode.WHILE_STATEMENT:
				WhileStatement whileStatement = (WhileStatement) astNodes
						.get(0);
				return whileStatement.getExpression().toString();
			}
		}
		return "null";
	}

	private  List<ASTNode> getASTNodes(HashMap<ASTNode, Line> map) {
		List<ASTNode> nodes = new LinkedList<ASTNode>();
		for (Entry<ASTNode, Line> entry : map.entrySet())
			nodes.add(entry.getKey());
		return nodes;
	}

/*	private class BranchNode{
		private int currentNode;
		private int successorNode;
		private String guard;
		private boolean decision;
		public BranchNode() {
			// TODO Auto-generated constructor stub
		}
		public BranchNode(int currentNode,int successorNode,String guard,boolean decision) {
			setCurrentNode(currentNode);
			setSuccessorNode(successorNode);
			setDecission(decision);
			setGuard(guard);
			
		}
		
		
		public void setCurrentNode(int currentNode) {
			this.currentNode = currentNode;
		}
		public void setDecission(boolean decision) {
			this.decision = decision;
		}
		public void setGuard(String guard) {
			this.guard = guard;
		}
		public void setSuccessorNode(int successorNode) {
			this.successorNode = successorNode;
		}
		public boolean isDecission() {
			return decision;
		}
		
		public int getCurrentNode() {
			return currentNode;
		}
		public int getSuccessorNode() {
			return successorNode;
		}
		public String getGuard() {
			return guard;
		}
		public boolean getDecision(){
			return this.decision;
		}
		
		public void print(){
			System.out.println(this.getCurrentNode()+ " " +
							   this.getSuccessorNode() + " "+
								this.getGuard() + " " +
							   this.getDecision());
		}
	}*/
}

package userinterface;
import javax.swing.JFrame;

import java.awt.Toolkit;
import java.awt.Dimension;

import javax.swing.JButton;

import java.awt.BorderLayout;

import javax.swing.JTextField;

import java.awt.SystemColor;
import java.awt.Point;
import java.awt.Insets;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFileChooser;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;

import java.awt.Label;
import java.awt.Panel;

import main.activator.Activator;
import main.activator.MainController;
import main.codeflow.ASTParsing;
import main.codeflow.ControlFlowGraph;
import main.codeflow.ControlFlowGraphCollection;
//import net.miginfocom.swing.MigLayout;



import javax.swing.JLabel;

import adt.graph.AbstractPath;

import com.jgoodies.forms.factories.DefaultComponentFactory;

import domain.constants.GraphCoverageCriteriaId;
import domain.controllers.TestRequirementController;

import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.UIManager;
import javax.swing.border.MatteBorder;
import javax.swing.event.ListDataListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.html.FormSubmitEvent.MethodType;
import javax.swing.JPanel;

import java.awt.Font;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.List;

import javax.swing.JList;
import javax.swing.JCheckBox;
import javax.swing.AbstractListModel;
import javax.swing.border.TitledBorder;

import org.eclipse.jdt.core.dom.CompilationUnit;

import testsuitgeneration.MethodTestSuit;

import javax.swing.JRadioButton;
import javax.swing.ImageIcon;

public class SQAkitGUI extends JFrame {

	private static ArrayList<String> javaFiles = new ArrayList<>();
	private ArrayList<String> desiredJavaFiles = new ArrayList<>();
	private ArrayList<String> absolutePath = new ArrayList<>();
	private GraphCoverageCriteriaId coverageCriteriaId ;
	
	String AbsoluteDirectory=null;
	
	@SuppressWarnings("unchecked")
	public SQAkitGUI() {
		
		setVisible(true);
		getContentPane().setBackground(SystemColor.activeCaption);
		setPreferredSize(new Dimension(1000, 780));
		setTitle("SQA Kit");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Asad Chachar\\Desktop\\logo.png"));

		JButton btnImportProject = new JButton("Import JAVA Files");


		final JPanel panel_1 = new JPanel();
		panel_1.setVisible(true);
		//JScrollPane scrolpane1=new JScrollPane(list);	
		final JList list= new JList(javaFiles.toArray());
		final JList list_1= new JList(desiredJavaFiles.toArray());
		final JPanel applyTest = new JPanel();
		applyTest.setVisible(true);
		applyTest.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Coverage Critera", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLACK));
										
										final JRadioButton rdbtnPathCoverage = new JRadioButton("Path Coverage",false);
										
										final JRadioButton rdbtnBranchCoverage = new JRadioButton("Branch Coverage",false);
										
										final JRadioButton rdbtnStatementCoverage = new JRadioButton("Statement Coverage",false);
										
										final ButtonGroup buttonGroup = new ButtonGroup();
										buttonGroup.add(rdbtnBranchCoverage);
										buttonGroup.add(rdbtnPathCoverage);
										buttonGroup.add(rdbtnStatementCoverage);
										
										GroupLayout gl_applyTest = new GroupLayout(applyTest);
										gl_applyTest.setHorizontalGroup(
											gl_applyTest.createParallelGroup(Alignment.LEADING)
												.addGroup(Alignment.TRAILING, gl_applyTest.createSequentialGroup()
													.addContainerGap()
													.addComponent(rdbtnPathCoverage)
													.addPreferredGap(ComponentPlacement.RELATED, 120, Short.MAX_VALUE)
													.addComponent(rdbtnBranchCoverage)
													.addGap(106)
													.addComponent(rdbtnStatementCoverage)
													.addGap(17))
										);
										gl_applyTest.setVerticalGroup(
											gl_applyTest.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_applyTest.createSequentialGroup()
													.addContainerGap()
													.addGroup(gl_applyTest.createParallelGroup(Alignment.BASELINE)
														.addComponent(rdbtnPathCoverage)
														.addComponent(rdbtnStatementCoverage)
														.addComponent(rdbtnBranchCoverage))
													.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										);
										applyTest.setLayout(gl_applyTest);
		
		JButton btnNewButton_2 = new JButton("Generate Test Data");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Start Testing");
				initializeAbsolutePath();
				try {
//					myMain();
					if(rdbtnBranchCoverage.isSelected())
						coverageCriteriaId = GraphCoverageCriteriaId.EDGE;
					else if(rdbtnStatementCoverage.isSelected())
						coverageCriteriaId = GraphCoverageCriteriaId.NODE;
					else {
						coverageCriteriaId = GraphCoverageCriteriaId.PRIME_PATH;
					}
					MainController m=new MainController(getAbsolutePath(),coverageCriteriaId);
				} 
				catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
//				
			}
		});
		
		JLabel lblLogo = new JLabel("logo");
		lblLogo.setIcon(new ImageIcon("C:\\Users\\Asad Chachar\\Desktop\\logo.png"));
		
		JLabel lblSqaKit = new JLabel("");
		lblSqaKit.setIcon(new ImageIcon("C:\\Users\\Asad Chachar\\Desktop\\SQA.png"));
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblSqaKit)
									.addGap(18)
									.addComponent(lblLogo, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
									.addGap(27))
								.addComponent(panel_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 731, Short.MAX_VALUE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addComponent(btnImportProject, GroupLayout.DEFAULT_SIZE, 731, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addComponent(applyTest, GroupLayout.DEFAULT_SIZE, 731, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(21)
							.addComponent(btnNewButton_2, GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSqaKit)
						.addComponent(lblLogo, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnImportProject, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addGap(13)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 378, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(applyTest, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNewButton_2, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(16, Short.MAX_VALUE))
		);
		
				
		JButton btnNewButton = new JButton(">>");
		btnNewButton.setToolTipText("Select");
		btnNewButton.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
		//		System.out.println(">>>"+list.getSelectedValue().toString());
				if(!(list.getSelectedValue() == null))
				addToJlist(list_1,list.getSelectedValue().toString());
			}

		
		});
		
		JLabel lblNewLabel_3 = new JLabel("Imported Java Files");
		
		JLabel lblNewLabel_4 = new JLabel("Selected Java Files");
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setViewportBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		
		JButton btnNewButton_1 = new JButton("<<");
		btnNewButton_1.setToolTipText("Remove");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!(list_1.getSelectedValue() == null))
				removeFromJlist(list_1,list_1.getSelectedValue().toString());
			}
		});
		
	
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(20)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_3))
					.addGap(18)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnNewButton_1, GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE)
						.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE))
					.addGap(18)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel_4, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE)
						.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
					.addGap(18))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel_4)
								.addComponent(lblNewLabel_3))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 323, GroupLayout.PREFERRED_SIZE)
								.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 323, GroupLayout.PREFERRED_SIZE))
							.addGap(24))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
							.addGap(154))))
		);
	//	final JList list_1 = new JList(desiredJavaFiles.toArray());
		scrollPane_1.setViewportView(list_1);
	//	final JList list = new JList(javaFiles.toArray());
		scrollPane.setViewportView(list);
		panel_1.setLayout(gl_panel_1);

		
		btnImportProject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JFileChooser chooser = new JFileChooser("E:\Study\VII\Project I (FYP)\SQAK\SQAK\src");
				chooser.setCurrentDirectory(new java.io.File(".java"));
				chooser.setDialogTitle("Import Project ");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);
				
				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {

				} else {
					System.out.println("No Selection ");
				}
				
				
				String files = null;
				if(!(chooser.getSelectedFile()==null)){
				File folder = new File(chooser.getSelectedFile().toString());
				File[] listOfFiles = folder.listFiles();
				AbsoluteDirectory=chooser.getSelectedFile().toString();
//				System.out.println("Directory: "+AbsoluteDirectory);
				
				parseDir(folder);
				int count = 0;
				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].isFile()) {
						files = listOfFiles[i].getName();
						if (files.endsWith(".java") || files.endsWith(".JAVA")) {
							
							javaFiles.add(files);
	/*						javaFiles[count] = files.toString();
							System.out.println(javaFiles[count]);
	*/						count++;
							files = files + "\n";
						}
					}
				}
	//			
	//			AbsolutePath();
				
				updateList(list);
				panel_1.setVisible(true);
				applyTest.setVisible(true);
				}
			}
	
			
		});
		
		
		btnImportProject.setMargin(new Insets(1, 0, 5, 8));
		groupLayout.setAutoCreateGaps(true);
		groupLayout.setAutoCreateContainerGaps(true);
		getContentPane().setLayout(groupLayout);
	}
	@SuppressWarnings({ "unchecked", "deprecation" })
	private void updateList(JList list) {
//		System.out.println(javaFiles);
		list.setListData(javaFiles.toArray());
	}
	private void addToJlist(JList list_1, String javaFile) {
		if(!desiredJavaFiles.contains(javaFile))
			desiredJavaFiles.add(javaFile);
		
		list_1.setListData(desiredJavaFiles.toArray());
//		System.out.println("Updated:" + desiredJavaFiles);
		
	}
	
	private	void initializeAbsolutePath()
	{
		String temp=null;
		for (int i=0; i<desiredJavaFiles.size();i++ )
		{
			temp=AbsoluteDirectory+"\\"+desiredJavaFiles.get(i);
			absolutePath.add(temp);
		}
//		System.out.println(absolutePath);
	}
	
	public ArrayList<String> getAbsolutePath() {
		return absolutePath;
	}
	public static void parseDir(File dirPath)
    {
		int Counter=1;

        File files[] = null;
        if(dirPath.isDirectory())
        {
            files = dirPath.listFiles();
            for(File dirFiles:files)
            {

                if(dirFiles.isDirectory())
                {
                    parseDir(dirFiles);
                }
                else
                {
                    if(dirFiles.getName().endsWith(".java")|| dirFiles.getName().endsWith(".JAVA"))
                    {
                    	javaFiles.add(dirFiles.getName());
//                    	System.out.println(Counter+" " + dirFiles.getName());
                    	Counter++;
                    }
                }
            }
        }  
   }
	private void removeFromJlist(JList list, String string ){
		if(!string.equals(null)){
		desiredJavaFiles.remove(string);
		list.setListData(desiredJavaFiles.toArray());
		}
	}
	
	public static void main(String args[]) {
		SQAkitGUI sqa = new SQAkitGUI();
		sqa.setSize(730, 730);
		sqa.setVisible(true);

		 try {
	            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
	                if ("Nimbus".equals(info.getName())) {
	                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
	                    break;
	                }
	            }
	        } catch (ClassNotFoundException ex) {
	            java.util.logging.Logger.getLogger(SQAkitGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        } catch (InstantiationException ex) {
	            java.util.logging.Logger.getLogger(SQAkitGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        } catch (IllegalAccessException ex) {
	            java.util.logging.Logger.getLogger(SQAkitGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
	            java.util.logging.Logger.getLogger(SQAkitGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	        }

	}
}

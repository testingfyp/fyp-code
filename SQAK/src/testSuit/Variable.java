package testSuit;

import java.util.ArrayList;

public class Variable {
	private String variableName ;
	private String variableType ;

	public Variable(){
		this.variableType = null;
		this.variableName = null;
	}
	public Variable(String name) {
		this.variableName = name;
	}

	public Variable(String variableName, String variableType) {
		super();
		this.variableName = variableName;
		this.variableType = variableType;
	}

	public String getVariableName() {
		
		return variableName;
	}

	public String getVariableType() {
		return variableType;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	public void setVariableType(String variableType) {
		this.variableType = variableType;
	}
	public boolean isEqual(Variable v1){
		if(v1.getVariableName().equals(this.getVariableName()) &&
		   v1.getVariableType().equals(this.getVariableType()) && !v1.equals(null)
		   )
		{
			return true;
		}
		
		return false;
	}
	public boolean isContains(ArrayList<Variable> varList, Variable var){
		
		for(Variable v : varList){
			if(var.isEqual(v)){
				return true;
			}
		}
		return false;
	}

}

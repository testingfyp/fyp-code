package testSuit;

import java.util.ArrayList;
import java.util.List;

public class BackTracker {
	private Problem problem;
	//private static Assignment assignment = null;
	private int initialValue = 0;
	private int fixedValue=0;
	private int check=0;
	private boolean ownAssigned=false;
	Boolean status=null;
	public BackTracker(Problem problem) {
		this.problem = problem;
	}

	public BackTracker(ArrayList<Constraint> constraint,ArrayList<Variable> variableList)
	{
		this.problem = new Problem(variableList, constraint);
	}

	public Assignment BackTrackingSearch() {
		Assignment assignment = Assignment.getAssignmentObject();
		if(!this.problem.getListOfVariables().isEmpty()){
			assignment.setAssignedValue(this.problem.getListOfVariables().get(0),initialValue);
			assignment = RecursiveBackTrack(this.problem, assignment);
		}
		else{
			System.out.println("parameters are empty");
		}
		return assignment;
	}
	
	
	boolean flag = false;
	public Assignment RecursiveBackTrack(Problem problem, Assignment assignment)
	{
		ownAssigned=false;
		Assignment result = null;

		if (assignment.isComplete(problem.getListOfVariables())){
//			System.out.println("***** Complete one Assignments *****");
			 System.out.print("( ");
			for (Variable v : problem.listOfVariables) {
				 System.out.print(v.getVariableName()/*+"		Type: "+v.getVariableType()*/+" : "+assignment.getAssignedvalue(v)+" ");
			}
			 System.out.println(" )");
			 
			return assignment;
		} 
	else {
			Variable var = selectUnassignedVariable(problem, assignment);	
	
			//************************************ Confidential **************************************************
			
			List<Constraint> tempConstraintList=this.problem.getListOfConstraint(var);
			
			for (Constraint tempConstraint : tempConstraintList) 
			{
				Variable tempVar1=tempConstraint.getVariable1();
				Variable tempVar2=tempConstraint.getVariable2();
		//		Integer temp1=null,temp2=null;
				if(Assignment.getAssignmentObject().getAssignedvalue(tempVar1)!=null)
				{
					initialValue= Assignment.getAssignmentObject().getAssignedvalue(tempVar1);
					assignment.setAssignedValue(var, initialValue);
				}
				else if( Assignment.getAssignmentObject().getAssignedvalue(tempVar2)!=null)
				{					
					initialValue= Assignment.getAssignmentObject().getAssignedvalue(tempVar2);
					assignment.setAssignedValue(var, initialValue);
				}
				else		// Both Variables returned null
				{
					fixedValue=0;
					initialValue=fixedValue;
					assignment.setAssignedValue(tempVar2, fixedValue);
					assignment.setAssignedValue(var, initialValue );
					ownAssigned=true;
				}
				
				if (assignment.isConsistent(problem.getListOfConstraint(var))) {
					result = RecursiveBackTrack(problem, assignment);
				}
				else
				{
					while(TrackVariables(assignment, result, var, initialValue).equals(false)&&fixedValue<50)
					{
						if(ownAssigned==true)
						{
							fixedValue += 10;
							initialValue=fixedValue;
							assignment.setAssignedValue(tempVar2, fixedValue);
							assignment.setAssignedValue(var, initialValue);
							if (assignment.isConsistent(problem.getListOfConstraint(var))) {
								result = RecursiveBackTrack(problem, assignment);
								break;
							}
						}
					}
					if(fixedValue>=50)
					{
						while(TrackVariables(assignment, result, var, initialValue).equals(false)&&fixedValue>0)
					{
						if(ownAssigned==true)
						{
							fixedValue -= 10;
							initialValue=fixedValue;
							assignment.setAssignedValue(tempVar2, fixedValue);
							assignment.setAssignedValue(var, initialValue);
							if (assignment.isConsistent(problem.getListOfConstraint(var))) {
								result = RecursiveBackTrack(problem, assignment);
								break;
							}
						}
					}
					}
					//break;
				}
					
				assignment.removeAssignedValue(var);
			}
			}
					
			//***************************************************************************************************
			
		
		return result;
	}
	
	
	public Boolean TrackVariables(Assignment assignment, Assignment result, Variable var, int initialValue)
	{
		status=false;
		{
			check=initialValue+10;
			// for one upper boundary	
			assignment.setAssignedValue(var,check);
			int i=0;
			while(!assignment.isConsistent(problem.getListOfConstraint(var))&&i<10) {
				check=initialValue+10;
				i++;
				assignment.setAssignedValue(var, check);
			}
			if(i<10)
			{
				i=0;
				status=true;
				result = RecursiveBackTrack(problem, assignment);
				return true;
			}
			else
			{
				i=0;
				check=initialValue-10;// for one lower boundary	
			
				assignment.setAssignedValue(var, check);
				while(!assignment.isConsistent(problem.getListOfConstraint(var))&&i>10) {
					check=initialValue+10;
					i++;
					assignment.setAssignedValue(var, check);
				}
				if(i<10)
				{
					i=0;
					status=true;
					result = RecursiveBackTrack(problem, assignment);
					return true;
				}
				else
				{
					return false;
				}
			}
			
		}
//		return null;
	}

	public Variable selectUnassignedVariable(Problem problem, Assignment assignment) {
		for (Variable v : problem.listOfVariables) {
			if (!assignment.doesAssignedValueExist(v)) {
				return v;
			}
		}
		return null;
	}

	public ArrayList<Integer> getDomainValues(Variable var,
			Assignment assignment, Problem problem) {
		return this.problem.getDomain(var).getDomainList();
	}
}

package testSuit;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class Assignment {
	
	private List<Variable> listOfVariables;
	private Hashtable<Variable,Integer> assignedValues;
	private static Assignment assignment= null;
	
	public Assignment() {
		listOfVariables=new ArrayList<Variable>();		//why?
		assignedValues=new Hashtable<Variable, Integer>();
	}
	
	// ************************* Assignment Singleton ********************************
	public static Assignment getAssignmentObject() {
		if (assignment == null) {
			assignment = new Assignment();
			return assignment;
		}
		return assignment;
	}

	public void setAssignedValue(Variable v, Integer key)
	{
	//	System.out.print("calls"+v.getVariableName());
		int a=0;
		for(Map.Entry<Variable,Integer> entry: assignedValues.entrySet()){
            if(v.isEqual((Variable)entry.getKey())){
            	listOfVariables.add(v);
    			assignedValues.put(v,key);
    			a+=1;
    			break;            //breaking because its one to one map		
    		}   
		}	
		if(a==0)
		{
			assignedValues.remove(v);
			listOfVariables.add(v);
			assignedValues.put(v,key);
		}
	}
	public void printHashTable()
	{
//		System.out.println(">>>"+assignedValues.size());
		for(Map.Entry<Variable,Integer> entry: assignedValues.entrySet()){
			Variable v=(Variable)entry.getKey();
			System.out.println(v.getVariableName());
			System.out.println(entry.getValue());
		}
	}
	
	public Integer getAssignedvalue(Variable v)
	{
		Integer value=null;
		/*if(assignedValues.contains(v))
		{
			assignedValues.;
		}*/
		for(Map.Entry<Variable,Integer> entry: assignedValues.entrySet()){
            if(v.isEqual((Variable)entry.getKey())){
                value = (Integer) entry.getValue();
                return value;
               //breaking because its one to one map
            }
        }
		return value;
	}
	
	public void removeAssignedValue(Variable var)
	{
		if(doesAssignedValueExist(var))
		{
			listOfVariables.remove(var);
			assignedValues.remove(var);
		}
	}
	public Boolean doesAssignedValueExist(Variable v)
	{
		for(Map.Entry entry: assignedValues.entrySet())
		{
            if(v.equals(entry.getKey())){
            if(!entry.getValue().equals(null))
            {
            	return true;
            }
          }
		}
		return false;
	}
	
	public Boolean isConsistent(List<Constraint> cons)
	{
//		System.out.print(cons.get(0).getOperator());
	for(int i=0;i<cons.size();i++)
		{
			Constraint c=cons.get(i);
			if(!c.isSatisfying(this))
			{
				return false;
			}
		}	
			return true;
	}
	public Boolean isComplete(List<Variable> vars)
	{
		for (Variable var : vars) {
			if(!doesAssignedValueExist(var))
			{
				return false;
			}
		}
		return true;
	}
	//************************************************************************************
public Boolean isBaseCondition(List<Variable> vars, Hashtable<Variable, ArrayList<Constraint>> dictionary)
{
	Constraint c=new Constraint();
	
//	for (Variable var : vars) {  ( )
		
	//	if(doesAssignedValueExist(var))	{
			for(Map.Entry<Variable, ArrayList<Constraint>> entry: dictionary.entrySet()){
		        //if(var.isEqual((Variable)entry.getKey()))
		        {
		        if(	doesAssignedValueExist((Variable)entry.getKey())&& getAssignedvalue((Variable)entry.getKey())!=-1)
		        {
		            ArrayList<Constraint>list = (ArrayList<Constraint>) entry.getValue();
		            for (int i = 0; i < list.size(); i++) {
		            	if(doesAssignedValueExist(list.get(i).getVariable1()) && 
		            			doesAssignedValueExist(list.get(i).getVariable2()) && getAssignedvalue(list.get(i).getVariable2())==-1
		            			&& getAssignedvalue(list.get(i).getVariable1())==-1)
		            	{
		            		if(!c.isConstraintSatisfies(list.get(i)))
		            		{
		            			return false;
		            		}
		            	}
		            }
		//           }
		          
		  //      }
		    }
		}
	}
	return true;
}

//***************************************************************************************
	public List<Variable> getListOfVariables() {
		return listOfVariables;
	}

	public void setListOfVariables(List<Variable> listOfVariables) {
		this.listOfVariables = listOfVariables;
	}

	public Hashtable<Variable, Integer> getAssignedValues() {
		return assignedValues;
	}

	public void setAssignedValues(Hashtable<Variable, Integer> assignedValues) {
		this.assignedValues = assignedValues;
	}

	public static Assignment getAssignment() {
		return assignment;
	}

	public static void setAssignment(Assignment assignment) {
		Assignment.assignment = assignment;
	}
	
}

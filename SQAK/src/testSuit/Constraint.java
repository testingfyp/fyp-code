package testSuit;

public class Constraint {
	private Variable variable1;
	private Variable variable2;
	private String operator;

	public Variable getVariable1() {
		return variable1;
	}

	public void setVariable1(Variable variable1) {
		this.variable1 = variable1;
	}

	public void setVariable2(Variable variable2) {
		this.variable2 = variable2;
	}

	public Variable getVariable2() {
		if(variable2!=null)
		{
		return variable2;
		}
		else
		return variable2;
	}

	public String getOperator() {
		return operator;
	}

	public Constraint() {
	}

	public Constraint(Variable variable1, Variable variable2, String operator) {
		super();
		this.variable1 = variable1;
		this.variable2 = variable2;
		this.operator = operator;
	}
	

public void setOperator(String operator) {
		this.operator = operator;
	}

	/*	public boolean isEqual(Variable v1, Variable v2){
		if(v1.getVariableName().equals(v2.getVariableName()) &&
		   v1.getVariableName().equals(v2.getVariableName())
		   )
		{
			return true;
		}
		
		return false;
	}*/
	public boolean isSatisfying(Assignment assignment) {
		Integer var1 = assignment.getAssignedvalue(this
				.variable1);
		
//		System.out.println(">>>Integer: "+var1);
		
		if (this.variable1.equals(null)) {
			return true;
		} else {
			Integer var2 = assignment.getAssignedvalue(this.variable2);
			String op = this.operator;
			if (var2 == null) {
				return true;
			}
			if (checkContraint(var1, var2, op)) {
				return true;
			} else {
				return false;
			}

		}
	}
	
	public boolean checkContraint(Integer var1, Integer var2, String op) {

		switch (op) {
		case ">":
			if (var1 > var2) {
				return true;
			}
			break;
		case "<":
			if (var1 < var2) {
				return true;
			}
			break;
		case "==":
			if (var1 == var2) {
				return true;
			}
			break;
		case ">=":
			if (var1 >= var2) {
				return true;
			}
			break;
		case "<=":
			if (var1 <= var2) {
				return true;
			}
			break;
		case "!=":
			if (var1 != var2) {
				return true;
			}
			break;

		}
		return false;
	}
	//*************************************************************************
	public Boolean isConstraintSatisfies(Constraint c)
	{
		Variable var1=c.getVariable1();
		Variable var2=c.getVariable2();
		String op=c.getOperator();
		Integer a=Assignment.getAssignmentObject().getAssignedvalue(var1);
		Integer b=Assignment.getAssignmentObject().getAssignedvalue(var2);
		return checkContraint(a, b, op);		
	}
	//***************************************************************************
}

package testSuit;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class Problem {
	 		List<Variable> listOfVariables;
	private List<Domain> listOfDomain = new ArrayList<Domain>();
	private List<Constraint> listOfConstraint;
	private Hashtable<Variable, ArrayList<Constraint>> dictionary= new Hashtable<Variable, ArrayList<Constraint>>();;

	// ******************************* Problem Constructor (The Problem Creator)
	// *****************************************
	public Problem() {
		this.listOfVariables = new ArrayList<Variable>();
		this.listOfDomain = new ArrayList<Domain>();
		this.listOfConstraint = new ArrayList<Constraint>();
		this.dictionary = new Hashtable<Variable, ArrayList<Constraint>>();
	}

	public Problem(List<Variable> listOfVariables, List<Constraint> listOfConstraint){
		super();
		this.listOfVariables = listOfVariables;
		this.listOfConstraint = listOfConstraint;
		
		for (int i = 0; i < this.listOfVariables.size(); i++) {
			this.listOfDomain.add(new Domain());
		}

		for (Variable var : this.listOfVariables) {
			ArrayList<Constraint> constraintListForVariable = new ArrayList<Constraint>();
			for (Constraint constraint : this.listOfConstraint) {
				if (var.isEqual(constraint.getVariable1()))
					//|| var.isEqual(constraint.getVariable2())) {
				{
					constraintListForVariable.add(constraint);
				}
				if(var.getVariableName().equals(constraint.getVariable2().getVariableName())&&var.getVariableType().equals(constraint.getVariable2().getVariableType()))
				{
//					constraint.setOperator(constraint.getOperator());
					//constraint=swapConstraint(constraint);
					constraintListForVariable.add(constraint);	
				}
			}
			dictionary.put(var, constraintListForVariable);
		}
	}
		public Constraint swapConstraint(Constraint cons)
		{
			if(cons.getOperator().contains(">"))
			{
				cons.getOperator().replace(">", "<");
				//Variable temp=cons.getVariable1();
				//cons.setVariable1(cons.getVariable2());
				//cons.setVariable2(temp);
			}
			if(cons.getOperator().contains("<"))
			{
				cons.getOperator().replace("<", ">");
				/*Variable temp=cons.getVariable1();
				cons.setVariable1(cons.getVariable2());
				cons.setVariable2(temp);*/
			}
			return cons;
		}
		/*
		 * for(Map.Entry entry: dictionary.entrySet()){ Variable v1=(Variable)
		 * entry.getKey(); ArrayList<Constraint> c1=new
		 * ArrayList<Constraint>();// entry.getKey();
		 * c1=(ArrayList<Constraint>)entry.getValue();
		 * System.out.print(c1.get(0)
		 * .getVariable1().getVariableName()+":"+":"+c1
		 * .get(0).getOperator()+":"+
		 * c1.get(0).getVariable2().getVariableName()); }
		 */
	

	public List<Domain> getListOfDomain() {
		return listOfDomain;
	}

	public List<Constraint> getListOfConstraint() {
		return listOfConstraint;
	}

	public Hashtable<Variable, ArrayList<Constraint>> getDictionary() {
		return dictionary;
	}

	public List<Variable> getListOfVariables() {
		return listOfVariables;
	}

	public Domain getDomain(Variable var) {
		int index = this.listOfVariables.indexOf(var);
		return listOfDomain.get(index);
	}

	public List<Constraint> getListOfConstraint(Variable var) {
		for (Map.Entry entry : dictionary.entrySet()) {
			if (var.equals(entry.getKey())) {
				return (ArrayList<Constraint>) entry.getValue();
			}
		}
		return null;
	}
}
